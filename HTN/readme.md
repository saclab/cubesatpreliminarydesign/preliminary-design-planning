# HyperTensioN


```
cd /home/tgateau/git/saclab/preliminary-design-planning/HTN
source ../loadEnv.bash
plan design.hddl instance.hddl debug

```


# loadEnv

```
loadEnv.bash

```



# Graph of instance dependencies

## biblio 

[Decision Advisor](https://sites.google.com/site/brumbaughresearch/publications?authuser=0)



CubeSats
Obvious contraints: 

"Feature" ?

EO mission -> SSO
EO mission -> LEO

Radiation -> 600 < altitude < 900

Localisation -> Constellation
             -> 

Electromagnetic sensors -> LEO
                        -> polar

"Reactive" system 1Sat : 
  -> antenna covering all ground
  -> equatorial + antenna coveing
  -> data link with Iridium / Globalstar / Starlink / OneWeb

Data amount
 small -> UHF/VHF or data
 medium -> S-Band OR many stations
 huge -> S-band + many stations OR X-Band ...

Openings ? 
 -> Molnya orbit for EO in specific and temporary points





