# preliminary-design-planning

Welcome to the preliminary-design-planning repository!


# download into ~/git/saclab/ path

```
mkdir git && cd git && mkdir saclab && cd saclab
git clone https://gitlab.isae-supaero.fr/saclab/cubesatpreliminarydesign/preliminary-design-planning.git
```

# quickstart

```
cd ~/git/saclab/preliminary-design-planning/
source loadEnv.bash 
plan design.hddl instance.hddl debug
planviz design.hddl instance.hddl 
```

# Planner

## strategy

2 level of decision are available
1 - high level - ordering of task and call script to do the elemntary tasks
2 - direclty in plan : if uhf, will need less power than S but required less datarate

## Solver

We used: [HyperTension](https://github.com/Maumagnaguagno/HyperTensioN)

HyperTension is an HTN planner written in Ruby. It is developped by Maurício Cecílio Magnaguagno, Felipe Meneguzzi, Lavindra de Silva.

A big thanks to the authors!

@inproceedings{magnaguagno2020hypertension,
  title={HyperTensioN: A three-stage compiler for planning},
  author={Magnaguagno, Maur{\'\i}cio C and Meneguzzi, Felipe Rech and De Silva, Lavindra},
  booktitle={Proceedings of the 30th International Conference on Automated Planning and Scheduling (ICAPS), 2020, Fran{\c{c}}a.},
  year={2020}
}

For more information, please visit: https://github.com/Maumagnaguagno/HyperTensioN  to install it and learn how to use it.

