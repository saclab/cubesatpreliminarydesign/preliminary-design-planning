#!/bin/bash


cd ~/git/saclab/ssa/intro-htn

HTN_PATH=$(pwd)

#HyperTensioN execution command: 
function plan {
ruby ${HTN_PATH}/HyperTensioN/Hype.rb $1 $2 $3
}
alias plan='plan'

#Ruby dot:
function planviz {
plan $1 $2 dot
dot -Tpdf "$1".dot > "$1".pdf && 
evince "$1".pdf &
}
alias planviz='planviz'


export HTN_PATH
export -f planviz

echo $HTN_PATH
echo "HTN env loaded!"

cd ~/git/saclab/preliminary-design-planning/HTN

